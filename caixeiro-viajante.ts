import 'es6-shim'

import { Grafo, Vertice, Aresta } from './grafo'

interface ValorVerticeCaixeiroViajante {
    visitado: boolean
}

interface ValorArestaCaixeiroViajante {
    peso: number
}

interface VerticeCaixeiroViajante extends Vertice<ValorVerticeCaixeiroViajante> { }
interface ArestaCaixeiroViajante extends Aresta<ValorVerticeCaixeiroViajante, ValorArestaCaixeiroViajante> { }
class GrafoCaixeiroViajante extends Grafo<ValorVerticeCaixeiroViajante, ValorArestaCaixeiroViajante> { }

function inclueVertice(id: string): (aresta: Aresta<any, any>) => boolean {
    return (aresta) => {
        return aresta.vertice0.id === id || aresta.vertice1.id === id
    }
}

function menorPeso(aresta0: ArestaCaixeiroViajante, aresta1: ArestaCaixeiroViajante): number {
    return aresta0.valor.peso - aresta1.valor.peso
}

function somarPeso(peso: number, aresta: ArestaCaixeiroViajante): number {
    return peso + aresta.valor.peso
}

function outroVertice<T>(verticeId: string): (aresta: Aresta<T, any>) => Vertice<T> {
    return (aresta) => {
        if (aresta.vertice0.id === verticeId) {
            return aresta.vertice1
        }
        return aresta.vertice0
    }
}

function menorCaminho(caminhos: ArestaCaixeiroViajante[][]): ArestaCaixeiroViajante[] | undefined {
    let pesoMenorCaminho: number | undefined
    let menorCaminho: ArestaCaixeiroViajante[] | undefined

    for (const caminho of caminhos) {
        const peso = caminho.reduce(somarPeso, 0)
        if (pesoMenorCaminho === undefined || peso < pesoMenorCaminho) {
            pesoMenorCaminho = peso
            menorCaminho = caminho
        }
    }

    return menorCaminho
}

function encontrarMenorCicloHamilton(
    grafo: GrafoCaixeiroViajante, 
    verticeInicialId: string
): ArestaCaixeiroViajante[] | undefined {
    const caminhos: ArestaCaixeiroViajante[][] = []
    let caminho: ArestaCaixeiroViajante[] = []

    const vertices = grafo.vertices()
    const arestas = grafo.arestas()
    const verticeInicial = vertices.find(vertice => vertice.id === verticeInicialId)

    if (!verticeInicial) {
        throw `Vertice com id ${verticeInicialId} não encontrado.`
    }

    let vertice = verticeInicial

    while (caminhos.length < arestas.length) {
        vertice.valor.visitado = true

        let arestasVertice = arestas.filter(inclueVertice(vertice.id))
        arestasVertice = arestasVertice.sort(menorPeso)
        const vizinhos = arestasVertice.map(outroVertice<ValorVerticeCaixeiroViajante>(vertice.id))

        for (let i = 0; i < vizinhos.length; i++) {
            const aresta = arestasVertice[i]
            const vizinho = vizinhos[i]
            if (!vizinho.valor.visitado) {
                vertice = vizinho
                break
            }
        }

        caminhos.push(caminho)
    }

    return menorCaminho(caminhos)
}

const grafo = new GrafoCaixeiroViajante()
grafo.adicionarVertice('1', { visitado: false })
grafo.adicionarVertice('2', { visitado: false })
grafo.adicionarVertice('3', { visitado: false })
grafo.adicionarVertice('4', { visitado: false })
grafo.adicionarAresta('1', '2', { peso: 10 })
grafo.adicionarAresta('2', '3', { peso: 10 })
grafo.adicionarAresta('3', '4', { peso: 10 })
grafo.adicionarAresta('4', '1', { peso: 10 })

const menorCiclo = encontrarMenorCicloHamilton(grafo, '1')

console.log(menorCiclo)




