
import { Grafo } from './grafo'
import { prim } from './arvore-geradora-minima/prim'
import { kruskal } from './arvore-geradora-minima/kruskal'

const grafo = new Grafo<null, number>()
grafo.adicionarVertice('A', null)
grafo.adicionarVertice('B', null)
grafo.adicionarVertice('C', null)
grafo.adicionarVertice('D', null)
grafo.adicionarVertice('E', null)
grafo.adicionarVertice('F', null)
grafo.adicionarAresta('A', 'C', 7)
grafo.adicionarAresta('A', 'D', 2)
grafo.adicionarAresta('A', 'E', 10)
grafo.adicionarAresta('B', 'F', 2)
grafo.adicionarAresta('C', 'E', 9)
grafo.adicionarAresta('C', 'F', 3)
grafo.adicionarAresta('C', 'B', 3)
grafo.adicionarAresta('D', 'E', 7)
grafo.adicionarAresta('F', 'E', 8)
grafo.adicionarAresta('F', 'D', 4)

console.log(kruskal(grafo))
