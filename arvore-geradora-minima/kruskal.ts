import { Grafo, Aresta, Vertice } from '../grafo'
import { menorAresta } from './menor-aresta'

export function kruskal<T>(grafo: Grafo<T, number>): Aresta<T, number>[] {
    const _arestas = grafo.arestas()
    const _vertices = grafo.vertices()
    const s: Aresta<T, number>[] = []
    const q = _arestas.slice()
    let floresta: Vertice<T>[][] = _vertices.map(vertice => [vertice])

    while (q.length > 0) {
        const _menorAresta = menorAresta(q)
        const indiceAresta = q.findIndex(aresta => {
            return aresta.vertice0.id === _menorAresta.vertice0.id
                && aresta.vertice1.id === _menorAresta.vertice1.id
                || aresta.vertice0.id === _menorAresta.vertice1.id
                && aresta.vertice1.id === _menorAresta.vertice0.id
        })
        q.splice(indiceAresta, 1)

        const u = _menorAresta.vertice0
        const v = _menorAresta.vertice1

        const indiceArvoreU = floresta
            .findIndex(arvore => arvore.findIndex(vertice => vertice.id === u.id) !== -1)
        const indiceArvoreV = floresta
            .findIndex(arvore => arvore.findIndex(vertice => vertice.id === v.id) !== -1)

        if (indiceArvoreU !== indiceArvoreV) {
            const arvoreU = floresta[indiceArvoreU]
            const arvoreV = floresta[indiceArvoreV]

            const arvoreNova = arvoreU.concat(arvoreV)

            if (indiceArvoreV < indiceArvoreU) {
                floresta.splice(indiceArvoreU, 1)
                floresta.splice(indiceArvoreV, 1)
            }
            else {
                floresta.splice(indiceArvoreV, 1)
                floresta.splice(indiceArvoreU, 1)
            }

            floresta.push(arvoreNova)

            s.push(_menorAresta)
        }

        console.log(floresta.map(arvore => arvore.map(vertice => vertice.id)))
    }

    return s
}