import { Grafo, Aresta, Vertice } from '../grafo'
import { menorAresta } from './menor-aresta'

export function prim<T>(grafo: Grafo<T, number>): Aresta<T, number>[] {
    const _arestas = grafo.arestas()
    const s: Aresta<T, number>[] = []
    const q = grafo.vertices().slice()

    q.shift()

    while (q.length > 0) {
        const arestasPossiveis = _arestas
            .filter(aresta => {
                function contemVertice(vertice: Vertice<T>): boolean {
                    return q.find(_vertice => _vertice.id === vertice.id) !== undefined
                }
                const contemVertice0 = contemVertice(aresta.vertice0)
                const contemVertice1 = contemVertice(aresta.vertice1)
                return contemVertice0 && !contemVertice1
                    || contemVertice1 && !contemVertice0
            })

        const _menorAresta = menorAresta(arestasPossiveis)

        s.push(_menorAresta)

        let verticeNoQ = _menorAresta.vertice0
        if (q.find(vertice => vertice.id === verticeNoQ.id) === undefined) {
            verticeNoQ = _menorAresta.vertice1
        }

        const indiceVertice = q.findIndex(vertice => vertice.id === verticeNoQ.id)
        q.splice(indiceVertice, 1)
    }

    return s
}