import { Aresta } from '../grafo'

export function menorAresta<T>(arestas: Aresta<T, number>[]): Aresta<T, number> {
    return arestas.reduce((menor, aresta) => {
        if (aresta.valor < menor.valor) {
            return aresta
        }
        return menor
    }, arestas[0])
}