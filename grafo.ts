export interface Vertice<T> {
    readonly id: string
    valor: T
}

export interface Aresta<T, U> {
    vertice0: Vertice<T>
    vertice1: Vertice<T>
    valor: U
}

export class Grafo<T, U> {
    private _vertices: Vertice<T>[] = []
    private _arestas: Aresta<T, U>[] = []

    vertices(): Vertice<T>[] {
        return this._vertices.slice()
    }

    arestas(): Aresta<T, U>[] {
        return this._arestas.slice()
    }

    adicionarVertice(id: string, valor: T) {
        this._vertices.push({ id, valor })
    }

    adicionarAresta(vertice0Id: string, vertice1Id: string, valor: U) {
        const vertice0 = this._vertices.find(vertice => vertice.id === vertice0Id)
        const vertice1 = this._vertices.find(vertice => vertice.id === vertice1Id)
        if (!vertice0) {
            throw `Vertice com id ${vertice0Id} não encontrado.`
        }
        if (!vertice1) {
            throw `Vertice com id ${vertice1Id} não encontrado.`
        }
        this._arestas.push({ vertice0, vertice1, valor })
    }
}